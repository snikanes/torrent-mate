const express = require("express");
const bodyparser = require("body-parser");
const cors = require("cors");

// Create new Express server
const api = express();

// Set up WebSockets for the server
const expressWs = require('express-ws')(api);

// Set up cors because the api is on another domain than the served file
let corsOptions = {
  origin: 'http://localhost:3000',
  credentials: true,
  methods: ['GET', 'PUT', 'POST'],
  optionsSuccessStatus: 200
};

api.use(cors(corsOptions));

// Middleware for easily parsing request bodies
api.use(bodyparser.json());
api.use(bodyparser.urlencoded({ extended: true }));

api.use('/torrents', require("./routers/torrentRouter.js"));

api.listen(1345);
console.log("API running...");
