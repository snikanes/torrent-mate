const express = require("express");
const Downloader = require("../DownloadController");
const WebSocketNotifier = require("../WebSocketNotifier.js");

const notifier = new WebSocketNotifier();

// Create new torrent downloader
const downloader = new Downloader();

// Make the notifier listen for download events
downloader.addListener(notifier);

// Create new router
const router = express.Router();

let listeners = [];

router.get("/", async (request, response) => {

	try {
		const inProgressTorrents = downloader.getTorrents();
		const torrentInfos = inProgressTorrents.map(torrent => {
			return {
				'name': torrent.name,
				'infoHash': torrent.infoHash,
				'length': torrent.length,
				'ready' : torrent.ready,
				'destroyed': torrent.destroyed,
				'done': torrent.done,
				'progress': torrent.progress
			}
		});

		response.status(200).json(torrentInfos);
	} catch(error) {
		response.status(500).send();
		console.error(error);
	}
});

router.post("/new", async (request, response) => {
	const magnetUri = request.body.postData.magnetUri;
	const mediaType = "TV";
	const name = "Fargo";

	if(!magnetUri) {
		return response.status(400).send({"reason": "Missing magnetUri"});
	} 
	if(!mediaType) {
		return response.status(400).send({"reason": "Missing mediaType"});
	}
	downloader.download(magnetUri, "/tmp/webtorrent", mediaType, name);
	response.status(201).send();			
});

router.post("/pause", async (request, response) => {
	const infoHash = request.body.postData.infoHash;
	if(!infoHash) {
		return response.status(400).send({"reason": "Missing infoHash"});
	}
	downloader.pauseDownload(infoHash);
	response.status(200).send();
});

router.post("/resume", async (request, response) => {
	const infoHash = request.body.postData.infoHash;
	if(!infoHash) {
		return response.status(400).send({"reason": "Missing infoHash"});
	}
	downloader.resumeDownload(infoHash);
	response.status(200).send();
});

router.ws("/", (ws, request) => {

	// Add the socket as a listener for future events
	notifier.addWebSocketListener(ws);

	// Send all current downloads, even paused ones
	downloader.getDownloads().forEach(statusMsg => ws.send(JSON.stringify(statusMsg)));

	// On error, we simply close the socket (dirty?)
  	ws.on('error', err => {
  		ws.close();
  	})
});

module.exports = router;