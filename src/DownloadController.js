const WebTorrent = require("WebTorrent");
const FileHandler = require("./FileHandler.js");

class DownloadController {

	constructor(listener) {
		this.client = new WebTorrent();
		this.listeners = [];
		this.pausedStatuses = new Map();
		this.fileHandler = new FileHandler("/Users/eirikvikanes/Downloads/Transmission");
	}

	getTorrents() {
		return this.client.torrents;
	}

	addListener(listener) {
		this.listeners.push(listener);
		console.log(this.listeners);
	}

	getDownloads() {
		return this.getTorrents().map(torrent => this.createStatusMsg(torrent));
	}

	download(magnetURI, downloadPath, mediaType, name) {

		this.client.add(magnetURI, torrent => {
			console.log(`[DownloadController]: Torrent ${torrent.name} started downloading.`);

			this.pausedStatuses[torrent.infoHash] = false;

			// Emitted when all the torrent files have been downloaded.
	  		torrent.on('done', () => {
	    		console.log(`[DownloadController]: Torrent ${torrent.name} finished downloading.`);
	    		this.fileHandler.handleFinishedDownload(torrent, mediaType, name);

	  		});

	  		// Emit an event
	  		torrent.on('download', (bytes) => {
	  			const msg = this.createStatusMsg(torrent);
	  			this.listeners.forEach(listener => listener.receivedBytes(msg));
			});
		});
	}

	pauseDownload(infoHash) {
		let torrent = this.client.get(infoHash);

		// Stops the torrent from connecting to new wires
		torrent.pause();
		// Removes the existing wires, actually pausing the torrent
		torrent.wires = [];

		// Set the paused status
		this.pausedStatuses[torrent.infoHash] = true;
	}

	resumeDownload(infoHash) {
		let torrent = this.client.get(infoHash);

		// Start using these wires
		torrent.resume();

		// Set the paused status
		this.pausedStatuses[torrent.infoHash] = false;
	}

	// Extract all the useful information to send to the browser
	createStatusMsg(torrent) {
		return {
			'name': torrent.name,
			'infoHash': torrent.infoHash,
			'length': torrent.length,
			'ready': torrent.ready,
			'destroyed': torrent.destroyed,
			'done': torrent.done,
			'totalDownloaded': torrent.downloaded,
			'downloadSpeed': torrent.downloadSpeed,
			'progress': torrent.progress,
			'paused': this.pausedStatuses[torrent.infoHash]
	  	}
	}
}

module.exports =  DownloadController;
