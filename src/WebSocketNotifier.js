const util = require('util')
const notificationInterval = require('./settings').notificationInterval

class WebSocketNotifier {

	constructor() {
		this.webSockets = [];
        this.timer = this.createSocketNotifier()
        this.mostRecentMsg = null
	}

	createSocketNotifier() {
        return setInterval(() => {
            if(this.mostRecentMsg) {
                this.broadcast(this.mostRecentMsg)
            }
        }, notificationInterval)
    }

	addWebSocketListener(ws) {
		this.webSockets.push(ws);
	}

	removeSocketListener(ws) {
		const index = this.webSockets.indexOf(ws);
		if(index > -1) {
			this.webSockets.splice(index, 1);
		} else {
			throw new Error("[WebSocketNotifier]: Socket not found in listener array");
		}

	}

	receivedBytes(msg) {
	    if(this.mostRecentMsg !== msg) {
            this.mostRecentMsg = msg
        }
	}

	broadcast() {
		this.webSockets.forEach(socket => {
			try {
				socket.send(JSON.stringify(this.mostRecentMsg));
			}
			// Write error, close the socket
			catch(err) {
				console.log("[WebSocketNotifier]: Closing not-open socket...");
				this.removeSocketListener(socket);
				socket.close();
			}
		});
	}
}

module.exports = WebSocketNotifier;
