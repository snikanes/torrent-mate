const fs = require("fs");

class FileHandler {

	constructor(storageDir) {
		this.storageDir = storageDir;
	}

	// Moves the files downloaded to the directory storage dir
	handleFinishedDownload(torrent, mediaType, name) {
		const rootPath = torrent.path;

		// Create new dir and move files there
		const mediaFolder = (!mediaType) ? '' : `/${mediaType}`;
		const newDir = this.storageDir + mediaFolder;
		const oldDir = rootPath;

		// Checks if media folder exists, creates if it doesn't. Then moves files into it
		fs.access(newDir, fs.constants.R_OK | fs.constants.W_OK, err => {
  			if(err) {
  				fs.mkdir(newDir, err => {
					if(err) {
						throw err
					} else {
						fs.mkdir(newDir + `/${torrent.name}`, err => {
							if(err) {
								throw err;
							} else {
								this.moveFiles(torrent, oldDir, newDir + `/${torrent.name}`);
							}
						});
					}
  				});
  			} else {
  				fs.mkdir(newDir + `/${torrent.name}`, err => {
					if(err) {
						throw err;
					} else {
						this.moveFiles(torrent, oldDir, newDir + `/${torrent.name}`);
					}
				});
  			}
		});
	}

	moveFiles(torrent, src, dest) {
		fs.rename(src, dest, err => {
			if(err) {
				throw err;
			} else {
				console.log(`[FileHandler]: Moved ${src} to ${dest}.`);
			}
		});
	}
}

module.exports = FileHandler;

// (async () => {
// 	const fh = new FileHandler("/Users/eirikvikanes/Downloads/Transmission");
// 	fh.handleFinishedDownload({
// 		'name': 'Fargo.S03E06.PROPER.720p.HDTV.x264-FLEET[rarbg]',
// 		'path': '/tmp/webtorrent/386247ba7f6150b991d771451d9e3cd94ba0b3d5'
// 	},'TV', 'Fargo');
// })();
